package com.app.radiobenderfmsg.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.app.radiobenderfmsg.Config;
import com.app.radiobenderfmsg.R;
import com.app.radiobenderfmsg.utilities.GDPR;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

public class ActivitySplash extends AppCompatActivity {

    private InterstitialAd interstitialAd;
    private AdRequest adRequest;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        if (Config.ENABLE_ADMOB_INTERSTITIAL_ADS_ON_LOAD) {
            loadInterstitialAd();
        }

        new CountDownTimer(Config.SPLASH_SCREEN_DURATION, 1000) {
            @Override
            public void onFinish() {
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
                finish();
                if (Config.ENABLE_ADMOB_INTERSTITIAL_ADS_ON_LOAD) {
                    if (interstitialAd!=null) {
                        interstitialAd.show(ActivitySplash.this);
                    }
                }
            }

            @Override
            public void onTick(long millisUntilFinished) {

            }
        }.start();

    }

    private void loadInterstitialAd() {
        Log.d("TAG", "showAd");
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        adRequest = new AdRequest.Builder().build();
        InterstitialAd.load(this,getResources().getString(R.string.admob_interstitial_unit_id), adRequest,
                new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd ad) {
                        interstitialAd = ad;
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        interstitialAd = null;
                    }
                });

        /*interstitialAd = new InterstitialAd(getApplicationContext());
        interstitialAd.setAdUnitId(getResources().getString(R.string.admob_interstitial_unit_id));
        interstitialAd.loadAd(GDPR.getAdRequest(this));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {

            }
        });*/
    }

}