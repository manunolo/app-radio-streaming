package com.app.radiobenderfmsg.utilities;

public interface PermissionsFragment {
    String[] requiredPermissions();
}
