package com.app.radiobenderfmsg.utilities;

public interface CollapseControllingFragment {
    boolean supportsCollapse();
}
